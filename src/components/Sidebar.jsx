import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faComments, faCogs } from '@fortawesome/free-solid-svg-icons';
import { NavLink } from 'react-router-dom'
import NavItem from './NavItem'

import userpic from '../default_userpic.png';

const Sidebar = () => {

  return (
    <aside className="main-sidebar">
      <section className="sidebar" style={{height: 'auto'}}>
        <div className="user-panel">
          <div className="pull-left image">
            <img src={userpic} className="img-circle" alt=""/>
          </div>
        </div>
        <ul className="sidebar-menu tree">
          <NavItem to='/feeds'>
            <NavLink to="/feeds">
              <FontAwesomeIcon icon={faComments} className="fa"/>
              <span>Feeds</span>
            </NavLink>
          </NavItem>
          <NavItem to='/settings'>
            <NavLink to="/settings">
              <FontAwesomeIcon icon={faCogs} className="fa"/>
              <span>Settings</span>
            </NavLink>
          </NavItem>
        </ul>
      </section>
    </aside>
  )
}

export default Sidebar;

import React from 'react';
import PropTypes from 'prop-types';

class NavItem extends React.Component {
  render() {
    var isActive = this.context.router.route.location.pathname === this.props.to;
    var activeClass = isActive ? 'active' : '';

    return(
      <li className={activeClass} >
        {this.props.children}
      </li>
    );
  }
}

NavItem.contextTypes = {
  router: PropTypes.object,
};

export default NavItem;

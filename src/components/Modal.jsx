import React, { Component } from 'react';
import PropTypes from 'prop-types'

class Modal extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const styles = {
      modal: {
        visibility: (this.props.visible) ? 'visible' : 'hidden',
        paddingRight: '15px',
        paddingLeft: '15px',
      }
    };

    return (
      <div id="myModal" className="modal" style={styles.modal} >
        <div className="modal-content">
          <div className="modal-body">
            {this.props.children}
          </div>
        </div>
        <div style={{position:'absolute', right:'15px', zIndex: 9999, width: '100%', height: '100%'}} onClick={this.props.onClickHandle}>
          <span className="close">&times;</span>
        </div>
      </div>
    )
  }
}

Modal.propTypes = {
  visible: PropTypes.bool.isRequired,
  onClickHandle: PropTypes.func.isRequired,
}

export default Modal;

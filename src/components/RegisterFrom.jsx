import React, { Component } from 'react'
import Input from './Input'
import ReCAPTCHA from 'react-google-recaptcha'
import PropTypes from 'prop-types'

class RegisterFrom extends Component {

  constructor(props) {
    super(props)

    this.state = {
      passwordConfirmed: false,
      captcha: false,
      passwordAlert: true,
      captchaAlert: true,
      email: '',
      password: '',
      passwordConfirmation: '',
      username: '',
      formDisabled: false,
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.dataValidate = this.dataValidate.bind(this);
  }

  onChange(value) {
    this.setState(value)
  }

  dataValidate() {
    const {passwordConfirmed, captcha} = this.state

    this.setState({
      passwordAlert: passwordConfirmed,
      captchaAlert: captcha
    })
    // return passwordConfirmed && captcha
    return true
  }


  handleSubmit(event) {
    event.preventDefault()
    if(this.dataValidate()) {
      const { email, password, passwordConfirmation, username } = this.state
      this.props.registration(email, username, password, passwordConfirmation)
    }
  }

  render() {
    const { passwordAlert, captchaAlert } = this.state
    return(
      <div className="col-md-6">
        <div className="box box-info">
          <div className="box-header with-border">
            <h3 className="box-title">Register</h3>
          </div>
          <form className="form-horizontal" onSubmit={this.handleSubmit}>
            <div className="box-body">
              <Input
                form_title = 'register'
                text = 'Email'
                onChange = {(value) => this.onChange({email: value})}
                type = 'email'
                required = {true}
              />
              <Input
                form_title = 'register'
                text = 'Username'
                onChange = {(value) => this.onChange({username: value})}
                required = {true}
                pattern =".{5,12}"
                title = '5 to 12 characters'
              />
              <Input
                form_title = 'register'
                text = 'Password'
                onChange = {(data, confirmed) => this.onChange({
                  password: data.value,
                  passwordConfirmation: data.confirm_value,
                  passwordConfirmed: confirmed
                })}
                type = 'password'
                confirm = {true}
                confirmed = {this.state.passwordConfirmed}
                required = {true}
                pattern =".{8,20}"
                title = '8 to 20 characters'
              />
              <div className="form-group">
                <ReCAPTCHA
                  sitekey='6LdiRXAUAAAAADvPIJPqUeXNcuNlVC8AJFHKhtEs'
                  onChange={() => this.setState({captcha: true})}
                  onErrored={() => this.setState({captcha: false})}
                  onExpired={() => this.setState({captcha: false})}
                  size='normal'
                  className='col-sm-push-3 col-sm-9'
                />
              </div>
              {(!passwordAlert || !captchaAlert) &&
              <div className="form-group has-error">
                {!passwordAlert && <span className="help-block col-sm-9 col-sm-push-3">Пароли не совпадают!</span>}
                {!captchaAlert && <span className="help-block col-sm-9 col-sm-push-3">Капча не пройдена!</span>}
              </div>
              }
            </div>
            <div className="box-footer">
              <button type="submit" className="btn btn-info">Register</button>
            </div>
          </form>
        </div>
      </div>
    )
  }

}

RegisterFrom.propTypes = {
  registration: PropTypes.func.isRequired
}

export default RegisterFrom;

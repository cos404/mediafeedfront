import React, { Component } from 'react';
import PropTypes from 'prop-types'
import Modal from '../components/Modal'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShare, faComments } from '@fortawesome/free-solid-svg-icons';
import { faThumbsUp } from '@fortawesome/free-regular-svg-icons';
import { faVk } from '@fortawesome/free-brands-svg-icons'

class FeedVk extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
      photoSrc: null,
      maxSizePhoto: null,
    };
  }

  toggleModal(callback = null) {
    const state = this.state.isOpen;
    this.setState({ isOpen: !state });

    let overflow = 'visible'
    state ? overflow = 'visible' : overflow = 'hidden'
    document.body.style.overflow = overflow;
    callback && callback();
  }

  openPhoto(photo) {
    this.toggleModal();
    this.setState({ photoSrc: photo, maxSizePhoto: photo.sizes[photo.sizes.length-1].url });
  }

  render() {
    const { posts, groups } = this.props;
    const { photoSrc, maxSizePhoto } = this.state;
    const groupsToHash = groups.reduce((obj, item) => {
      obj[item.id] = item
      return obj
    }, {})

    return (
      <div className="active">
        {
          posts.map((post) => {
            let source
            if(post.source_id < 0) source = groupsToHash[post.source_id*-1]
            else if(post.source_id > 0) return null
            else return null
            return (
              <div className="post" key={post.post_id}>
                <div className="user-block">
                  <img className="img-circle img-bordered-sm" src={source.photo_50} alt="user"/>
                  <span className="username">
                    <a href={`https://vk.com/${source.screen_name}`}>{source.name}</a>
                  </span>
                  <span className="description">Shared publicly - {Date(post.date)}</span>
                </div>
                <p>
                  {post.text}
                </p>
                <p>
                  {
                    post.attachments &&
                    post.attachments.map(attachment => {
                      let photo = attachment.photo;
                      return (
                        photo &&
                        <img
                          key={photo.id}
                          src={photo.sizes[0].url}
                          onClick={this.openPhoto.bind(this, photo)}
                          style={{cursor: 'pointer'}}
                          alt=""
                        />
                      )
                    })
                  }
                </p>
                <ul className="list-inline">
                  <li>
                    <a href={`https://vk.com/${source.screen_name}?w=wall${post.source_id}_${post.post_id}`} className="link-black text-sm">
                      <FontAwesomeIcon icon={faShare} className="fa margin-r-5" />
                      Share({post.reposts.count})
                    </a>
                  </li>
                  <li>
                    <a href={`https://vk.com/${source.screen_name}?w=wall${post.source_id}_${post.post_id}`} className="link-black text-sm">
                      <FontAwesomeIcon icon={faThumbsUp} className="fa margin-r-5" />
                      Like({post.likes.count})
                    </a>
                  </li>
                  <li className="pull-right">
                    <a href={`https://vk.com/${source.screen_name}?w=wall${post.source_id}_${post.post_id}`} className="link-black text-sm">
                      <FontAwesomeIcon icon={faComments} className="fa margin-r-5" />
                      Comments({post.comments.count})
                    </a>
                  </li>
                </ul>
              </div>
            )
          })
        }

        <Modal
          visible={this.state.isOpen}
          onClickHandle={this.toggleModal.bind(this, () => this.setState({ photoSrc: null, maxSizePhoto: null }) )}
        >
          <img src={maxSizePhoto} className="center-img" alt=""/>
          <div className="modal-info">
            { photoSrc &&
              <a href={`https://vk.com/photo${photoSrc.owner_id}_${photoSrc.id}`} target="_blank" className="btn btn-social-icon btn-vk">
                <FontAwesomeIcon icon={faVk} className="fa fa-vk" />
              </a>
            }
          </div>
        </Modal>
      </div>
    )
  }
}

FeedVk.propTypes = {
  posts: PropTypes.array.isRequired,
  groups: PropTypes.array.isRequired,
  profiles: PropTypes.array.isRequired,
}

export default FeedVk;

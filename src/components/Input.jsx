import React, { Component } from 'react';
import PropTypes from 'prop-types'

class Input extends Component  {
  constructor(props) {
    super(props);
    this.state = {
      value: null,
      confirm_value: null,
    }
  }

  onChange(e, new_state){
    this.setState(new_state, () => {
      if(this.props.confirm) { // If this field have a confirmation field
        if(this.state.value !== this.state.confirm_value) this.props.onChange({value: "", confirm_value: ""}, false)
        else this.props.onChange({value: this.state.value, confirm_value: this.state.confirm_value}, true)
      }
      else this.props.onChange(this.state.value)
    })
  }

  render() {
    let {text, type, confirm, form_title, pattern, required, title, labelColumn } = this.props
    let inputColumn;

    type = type || 'text'
    confirm = confirm || false
    required = required || false
    title = title ||''
    labelColumn = labelColumn || 3
    inputColumn = 12 - labelColumn

    return(
      <React.Fragment>
        <div className="form-group">
          <label htmlFor={text+'_'+form_title} className={`col-sm-${labelColumn} control-label`}>{text}</label>
          <div className={`col-sm-${inputColumn}`}>
            <input
              type={type}
              className="form-control"
              id={text+'_'+form_title}
              placeholder={text}
              onChange={(e) => this.onChange(e, {value: e.target.value})}
              pattern={pattern}
              required={required}
              title={title}
            />
          </div>
        </div>
        {confirm &&
        <div className="form-group">
          <label htmlFor={text+'_'+form_title} className={`col-sm-${labelColumn} control-label`}>Confirm {text}</label>
          <div className={`col-sm-${inputColumn}`}>
            <input
              type={type}
              className="form-control"
              id={text+'_confirm_'+form_title}
              placeholder={'Confirm '+text.toLowerCase()}
              onChange={(e) => this.onChange(e, {confirm_value: e.target.value})}
              required={required}
            />
          </div>
        </div>}
      </React.Fragment>
    )
  }
}

Input.propTypes = {
  text: PropTypes.string.isRequired,
  form_title: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  type: PropTypes.string,
  confirm: PropTypes.bool,
  pattern: PropTypes.string,
  required: PropTypes.bool,
  title: PropTypes.string,
  labelColumn: PropTypes.number,
}


export default Input;

// onChange(e, new_state){
//   console.log(new_state)
//   let target = e.target
//   this.setState(new_state, () => {
//     if(this.props.confirm){
//       if(this.state.value === this.state.confirm_value){
//         target.setCustomValidity('')
//         this.props.onChange(this.state.value, true)
//       }
//       else if(target.id.includes('confirm')) {
//         target.setCustomValidity(target.title)
//         this.props.onChange(this.state.value, false)
//       }
//       else {
//         this.props.onChange(this.state.value, false)
//       }
//     }
//     else {
//       this.props.onChange(this.state.value)
//     }
//     console.log(this.state)
//   })
// }

import React, { Component } from 'react';
import PropTypes from 'prop-types'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLink, faShare, faComments } from '@fortawesome/free-solid-svg-icons';
import { faThumbsUp } from '@fortawesome/free-regular-svg-icons';
import userpic from '../default_userpic.png';

class FeedAdd extends Component {

  constructor(props) {
    super(props)

    this.state = {
      vk_link: '',
    }

    this.handleSubmitVk = this.handleSubmitVk.bind(this);
  }

  handleSubmitVk(event) {
    event.preventDefault()
    this.props.vk_add(this.state.vk_link)
  }

  onChange(value) {
    this.setState(value)
  }

  render() {
    const vk_token_link = 'https://oauth.vk.com/authorize?client_id=6626406&scope=friends%2Cwall%2Cemail%2Coffline&redirect_uri=https%3A%2F%2Foauth.vk.com%2Fblank.html&display=page&response_type=token&v=5.80'
    return (
      <div className="active">
        <div className="post">
          <div className="user-block">
            <img className="img-circle img-bordered-sm" src={userpic} alt="user"/>
            <span className="username">
              vk.com
            </span>
            <span className="description">...</span>
          </div>
          <p>
            1. Go to: <a href={vk_token_link} target="_blank">vk.com</a><br/>
            2. After redirect input link in field.<br/>
            3. Click "Add"<br/>
          </p>
          <form className="form-horizontal" onSubmit={this.handleSubmitVk}>
            <div className="form-group margin-bottom-none">
              <div className="col-sm-9">
                <input
                  type='text'
                  className='form-control'
                  id='vk_link'
                  placeholder='Redirect link'
                  onChange={(e) => this.onChange({vk_link: e.target.value})}
                  required={true}
                />
              </div>
              <div className="col-sm-3">
                <button type="submit" className="btn btn-danger pull-right btn-block btn-sm">Add</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    )
  }
}

FeedAdd.propTypes = {
  vk_add: PropTypes.func.isRequired
}

export default FeedAdd;

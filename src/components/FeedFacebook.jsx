import React from 'react';
import PropTypes from 'prop-types'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLink, faShare, faComments } from '@fortawesome/free-solid-svg-icons';
import { faThumbsUp } from '@fortawesome/free-regular-svg-icons';
import userpic from '../default_userpic.png';

const FeedFacebook = (posts) => {
  return (
    <div className="active">
      <div className="post">
        <div className="user-block">
          <img className="img-circle img-bordered-sm" src={userpic} alt="user"/>
          <span className="username">
            <a href="# ">Jonathan Burke Jr.</a>
            <a href="# " className="pull-right btn-box-tool"><FontAwesomeIcon icon={faLink} className="fa" /></a>
          </span>
          <span className="description">Shared publicly - 7:30 PM today</span>
        </div>
        <p>
          Lorem ipsum represents a long-held tradition for designers, typographers and the like. Some people hate it and argue for its demise, but others ignore the hate as they create awesome tools to help create filler text for everyone from bacon lovers to Charlie Sheen fans.
        </p>
        <ul className="list-inline">
          <li>
            <a href="# " className="link-black text-sm">
              <FontAwesomeIcon icon={faShare} className="fa margin-r-5" />
              Share
            </a>
          </li>
          <li>
            <a href="# " className="link-black text-sm">
              <FontAwesomeIcon icon={faThumbsUp} className="fa margin-r-5" />
              Like
            </a>
          </li>
          <li className="pull-right">
            <a href="# " className="link-black text-sm">
              <FontAwesomeIcon icon={faComments} className="fa margin-r-5" />
              Comments(5)
            </a>
          </li>
        </ul>
        <input className="form-control input-sm" type="text" placeholder="Type a comment"/>
      </div>
    </div>
  )
}

FeedFacebook.propTypes = {
  posts: PropTypes.array.isRequired
}

export default FeedFacebook;

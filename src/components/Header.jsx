import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope } from '@fortawesome/free-regular-svg-icons';
import { faCogs } from '@fortawesome/free-solid-svg-icons';
import userpic from '../default_userpic.png';
import { NavLink } from 'react-router-dom'

const Header = () => {
  return (
    <header className="main-header">
      <NavLink to="/" className="logo">
        <span className="logo-mini">
          MF
        </span>
      </NavLink>
      <nav className="navbar navbar-static-top">
        <div className="navbar-custom-menu">
          <ul className="nav navbar-nav">
            <li className="dropdown messages-menu">
              <a href="# " className="dropdown-toggle">
                <FontAwesomeIcon icon={faEnvelope} className="fa" />
                <span className="label label-success">4</span>
              </a>
            </li>
            <li className="dropdown user user-menu">
              <a href="# " className="dropdown-toggle">
                <img src={userpic} className="user-image" alt="" />
                <span className="hidden-xs">Username</span>
              </a>
            </li>
            <li>
              <a href="# " className="dropdown-toggle">
                <FontAwesomeIcon icon={faCogs} className="fa" />
              </a>
            </li>
          </ul>
        </div>
      </nav>
    </header>
  )
}

export default Header;

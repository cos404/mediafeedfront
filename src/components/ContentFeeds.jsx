import React, { Component } from 'react';
import PropTypes from 'prop-types'
import FeedAdd from '../components/FeedAdd'
import FeedVk from '../components/FeedVk'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faVk } from '@fortawesome/free-solid-svg-icons';
// import FeedFacebook from '../components/FeedFacebook'
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

export default class ContentFeeds extends Component {
  constructor(props) {
    super(props);
  }

  getPosts = (index, lastIndex) => {
    if(!this.props.feeds[index]) return;
    let feed = this.props.feeds[index]
    if(!feed.posts) this.props.fetchPosts(feed.id);
  }

  render() {
    return (
      <section className="content">
        <div className="row">
          <div className="col-md-6">
            <Tabs
              className="nav-tabs-custom"
              selectedTabClassName="active"
              selectedTabPanelClassName="tab-content"
              onSelect={(index, lastIndex) => this.getPosts(index, lastIndex)}
            >
              <TabList className="nav nav-tabs">
                {
                  this.props.feeds.map((feed, index) => {
                    return <Tab key={feed.id} ><span>{feed.type}</span></Tab>
                  })
                }
                <Tab key={0} ><span><FontAwesomeIcon icon={faPlus} className="fa margin-r-5" /></span></Tab>
              </TabList>
                {
                  this.props.feeds.map(feed => {
                    return (
                      <TabPanel key={feed.id}>
                        { feed.posts && <FeedVk  posts={feed.posts} groups={feed.groups} profiles={feed.profiles}/> }
                      </TabPanel>
                    )
                  })
                }
                <TabPanel key={0}>
                  <FeedAdd vk_add={this.props.vk_add} />
                </TabPanel>
            </Tabs>
          </div>
        </div>
      </section>
    )
  }
}

ContentFeeds.propTypes = {
  feeds: PropTypes.array,
  fetchPosts: PropTypes.func.isRequired,
  vk_add: PropTypes.func.isRequired
}

import React, { Component } from 'react'
import Input from './Input'
import ReCAPTCHA from 'react-google-recaptcha'
import PropTypes from 'prop-types'

class LoginForm extends Component {
  constructor(props) {
    super(props)

    this.state = {
      captcha: false,
      captchaAlert: true,
      email: '',
      password: '',
      formDisabled: false,
    }

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  onChange(value) {
    this.setState(value)
  }

  dataValidate() {
    const { captcha } = this.state
    this.setState({ captchaAlert: captcha })
    return true
    // return captcha
  }

  handleSubmit(event) {
    if(this.dataValidate()) {
      const { email, password } = this.state
      this.props.login(email, password)
    }
    event.preventDefault()
  }

  render() {
    console.log(this.props)
    return(
      <div className="col-md-6">
        <div className="box box-primary">
          <div className="box-header with-border">
            <h3 className="box-title">Log in</h3>
          </div>
          <form className="form-horizontal" onSubmit={this.handleSubmit}>
            <div className="box-body">
              <Input
                form_title = 'login'
                text = 'Email'
                onChange = {(value) => this.onChange({email: value})}
                type = 'email'
                required = {true}
                labelColumn = {2}
              />
              <Input
                form_title = 'login'
                text = 'Password'
                onChange = {(value) => this.onChange({password: value})}
                type = 'password'
                required = {true}
                labelColumn = {2}
              />
              <div className="form-group">
                <ReCAPTCHA
                  sitekey='6LdiRXAUAAAAADvPIJPqUeXNcuNlVC8AJFHKhtEs'
                  onChange={() => this.setState({captcha: true})}
                  onErrored={() => this.setState({captcha: false})}
                  size='normal'
                  className='col-sm-push-2 col-sm-10'
                />
              </div>
              {!this.state.captchaAlert &&
              <div className="form-group has-error">
                <span className="help-block col-sm-10 col-sm-push-2">Капча не пройдена!</span>
              </div>
              }
            </div>
            <div className="box-footer">
              <button type="submit" className="btn btn-primary">Log in</button>
            </div>
          </form>
        </div>
      </div>
    )
  }
}

LoginForm.propTypes = {
  login: PropTypes.func.isRequired,
  user: PropTypes.object,
}

export default LoginForm

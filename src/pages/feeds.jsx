import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTachometerAlt } from '@fortawesome/free-solid-svg-icons';
import ContentFeeds from '../components/ContentFeeds'

// REDUX
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as FeedActions from '../actions/FeedActions';

class Feeds extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    const { feeds, feedActions } = this.props
    if(feeds.length === 0) feedActions.fetchPosts()
  }

  render() {
    return (
      <React.Fragment>
        <section className="content-header">
          <h1>Feeds</h1>
          <ol className="breadcrumb">
            <li>
              <a href="# ">
                <FontAwesomeIcon icon={faTachometerAlt} className="fa" />
                Home
              </a>
            </li>
            {/*<li><a href="# ">Examples</a></li>*/}
            <li className="active">Feeds</li>
          </ol>
        </section>
        <ContentFeeds
          feeds={this.props.feeds}
          fetchPosts={this.props.feedActions.fetchPosts}
          vk_add={this.props.feedActions.vk_add}
        />
      </React.Fragment>
    )
  }
}

function mapStateToProps(state) {
  return {
    feeds: state.feed.feeds,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    feedActions: bindActionCreators(FeedActions, dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Feeds)

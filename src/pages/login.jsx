import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types'
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { faTachometerAlt } from '@fortawesome/free-solid-svg-icons';
import LoginForm from '../components/LoginForm'
import RegisterFrom from '../components/RegisterFrom'
import { Redirect } from 'react-router-dom';
// REDUX
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as LogActions from '../actions/LogActions';

class Login extends Component {
  render() {
    if(this.props.user.isAuthorized) return <Redirect to='/'/>;

    return (
      <Fragment>
        <div style={{paddingTop: '50px'}}>
          <LoginForm login={this.props.logActions.login} user={this.props.user}/>
          <RegisterFrom registration={this.props.logActions.registration}/>
        </div>
      </Fragment>
    )
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
  }
}

function mapDispatchToProps(dispatch) {
  return {
    logActions: bindActionCreators(LogActions, dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)

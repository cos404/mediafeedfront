import axios from 'axios';
import {
  FETCH_POSTS_REQUEST,
  FETCH_POSTS_FAIL,
  FETCH_POSTS_SUCCESS,
  VK_ADD_REQUEST,
  VK_ADD_FAIL,
  VK_ADD_SUCCESS
} from '../constants/ActionTypes';

export function fetchPosts(id = null) {
  return (dispatch) => {
    dispatch({
      type: FETCH_POSTS_REQUEST,
      payload: id
    })
    getPosts(id, dispatch)
  }
}

function getPosts(id, dispatch) {
  const URL = "http://localhost:8000/api/v1/networks";
  return axios({
    method: 'GET',
    url: URL,
    headers: {'Authorization': localStorage.getItem('access_token')},
    params: { id: id }
  })
  .then(response => {
    if(response.data) {
      dispatch({
        type: FETCH_POSTS_SUCCESS,
        payload: {
          id: id,
          feed: response.data
        }
      })
    }
  })
  .catch((error) => {
    dispatch({
      type: FETCH_POSTS_FAIL,
      payload: {
        message: error
      }
    })
  });
}

export function vk_add(link) {
  const URL = 'http://localhost:8000/api/v1/networks'
  return (dispatch) => {
    dispatch({
      type: VK_ADD_REQUEST,
      link: link
    })

    axios({
      method: 'POST',
      url: URL,
      data: { network: { url: link }},
      headers: {'Authorization': localStorage.getItem('access_token')}
    })
    .then(response => {
      dispatch({
        type: VK_ADD_SUCCESS,
        payload: {
          type: response.data.type,
          id: response.data.id
        }
      })
    })
    .catch(response => {
      dispatch({
        type: VK_ADD_FAIL,
        error: response
      })
    })
  }
}


// return axios.get(URL, {
//   headers: {
//     "Authorization": "eyJhbGciOiJIUzI1NiJ9.eyJpZCI6MSwiZXhwIjoxNTM3Mzc2NDA5LCJ1c2VybmFtZSI6ImNvczQwNCIsImVtYWlsIjoiYWRtaW5AZ21haWwuY29tIiwicm9sZSI6MX0.JTKveRbBzzMsPAav51W7xnLgTl2y61ES5-W4nS6rH4Y"
//   },
//   params: { id: id }
// })

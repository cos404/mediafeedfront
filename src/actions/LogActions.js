import axios from 'axios'

import {
  REGISTRATION_REQUEST,
  REGISTRATION_FAIL,
  REGISTRATION_SUCCESS,
  SIGN_IN_REQUEST,
  SIGN_IN_FAIL,
  SIGN_IN_SUCCESS,
  PREPAIR_AUTH,
} from '../constants/ActionTypes'

export function registration(email, username, password, password_confirmation) {
  const URL = 'http://localhost:8000/api/v1/users/sign_up'
  const params = {
    user: { email, username, password, password_confirmation }
  }

  return (dispatch) => {
    dispatch({
      type: REGISTRATION_REQUEST,
      payload: {email, username, password, password_confirmation}
    })

    axios.post(URL, params)
    .then(response => {
      dispatch({
        type: REGISTRATION_SUCCESS,
        payload: {
          email: response.data.email
        }
      })
    })
    .catch((response) => {
      console.log(response.data)
      dispatch({
        type: REGISTRATION_FAIL,
        payload: {
          errors: response.data.errors
        }
      })
    })
  }
}

export function login(email, password) {
  const URL = 'http://localhost:8000/api/v1/users/sign_in'
  const params = { user: { email, password }}

  return (dispatch) => {
    dispatch({
      type: SIGN_IN_REQUEST,
      payload: {email}
    })

    axios.post(URL, params)
    .then(response => {
      const { email, username, role, access_token } = response.data
      localStorage.setItem('email', email);
      localStorage.setItem('access_token', access_token);
      localStorage.setItem('username', username);
      localStorage.setItem('role', role);

      dispatch({
        type: SIGN_IN_SUCCESS,
        payload: {
          email, username, role, access_token
        }
      })
    })
    .catch(response => {
      console.log(response.data)
      dispatch({
        type: SIGN_IN_FAIL,
        payload: {
          errors: response.data.errors
        }
      })
    })

  }
}

export function prepairAuth(data) {
  const {email, access_token, username, role} = data
  if(email && access_token && username && role) data.isAuthorized = true
  else {
    data = { isAuthorized: false }
    localStorage.clear()
  }

  return (dispatch) => {
    dispatch({
      type: PREPAIR_AUTH,
      payload: data
    })
  }
}

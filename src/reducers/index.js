import feed from './feed'
import user from './user'

import { combineReducers } from 'redux'

const rootReducer = combineReducers({
  feed,
  user,
})

export default rootReducer;

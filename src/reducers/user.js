import {
  REGISTRATION_REQUEST,
  REGISTRATION_FAIL,
  REGISTRATION_SUCCESS,
  SIGN_IN_REQUEST,
  SIGN_IN_FAIL,
  SIGN_IN_SUCCESS,
  PREPAIR_AUTH,
} from '../constants/ActionTypes';

const initialState = {
  isAuthorized: false,
};

export default (state = initialState, data) => {
  switch (data.type) {
    case REGISTRATION_REQUEST:
      return { ...state, errors: '' }
    case REGISTRATION_FAIL:
      return { ...state, errors: data.payload.errors }
    case REGISTRATION_SUCCESS:
      return { ...state, email: data.payload.email }
    case SIGN_IN_REQUEST:
      return { ...state, errors: '' }
    case SIGN_IN_FAIL:
      return { ...state, errors: data.payload.errors }
    case SIGN_IN_SUCCESS:
      return {
        ...state,
        email: data.payload.email,
        username: data.payload.username,
        access_token: data.payload.access_token,
        role: data.payload.role,
        isAuthorized: true
      }
    case PREPAIR_AUTH:
      return {
        ...state,
        ...data.payload
      }
    default:
      return state;
  }
}

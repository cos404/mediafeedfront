import {
  FETCH_POSTS_REQUEST,
  FETCH_POSTS_SUCCESS,
  FETCH_POSTS_FAIL,
  VK_ADD_REQUEST,
  VK_ADD_SUCCESS,
  VK_ADD_FAIL,
} from '../constants/ActionTypes'

const initialState = {
  feeds: [
    // { type: 'vk', id: 1},
    // { type: 'vk', id: 2}
  ]
};

export default (state = initialState, data) => {
  switch (data.type) {
    case FETCH_POSTS_REQUEST:
      return { ...state, error: '' }
    case FETCH_POSTS_SUCCESS:
      if(data.payload.id === null) {
        return {
          ...state,
          feeds: data.payload.feed
        }
      }

      const index = state.feeds.findIndex(v => v.id === data.payload.id)
      return {
        ...state,
        feeds: index === -1
        ? [...state.feeds]
        : [
          ...state.feeds.slice(0, index),
          data.payload.feed[index],
          ...state.feeds.slice(index + 1, state.feeds.length),
        ]
      }
    case FETCH_POSTS_FAIL:
      return { ...state, error: data }
    case VK_ADD_REQUEST:
      return { ...state }
    case VK_ADD_SUCCESS:
      return { ...state, feeds: [...state.feeds, {type: data.payload.type, id: data.payload.id}]}
    case VK_ADD_FAIL:
      return { ...state, error: data }
    default:
      return state;
  }
}

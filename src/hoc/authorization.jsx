import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';

// REDUX
import { bindActionCreators } from 'redux'
import * as LogActions from '../actions/LogActions';


const Athorization = (WrappedComponent) => {
  class WithAuthorization extends Component {
    static propTypes = {
      isAuthorized: PropTypes.bool
    };

    render() {
      const { isAuthorized } = this.props;
      if (!isAuthorized) {
        return <Redirect to='/login' />;
      }

      return <WrappedComponent {...this.props}/>;
    }
  }

  const mapStateToProps = (state) => (
    {
      isAuthorized: Boolean(state.user.isAuthorized)
    }
  );

  return connect(mapStateToProps)(WithAuthorization);
};



export default Athorization;

import Feeds from './pages/feeds'
import Login from './pages/login'
// import Home from './pages/home'

export const routes = [
  // {
  //   isNavBar: true,
  //   isExact: true,
  //   path: '/',
  //   name: 'Home',
  //   component: Home
  // },
  {
    isNavBar: true,
    isExact: true,
    isPrivate: true,
    path: '/feeds',
    name: 'Feeds',
    component: Feeds
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
]

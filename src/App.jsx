import React, { Component } from 'react';
import './App.css';
import PropTypes from 'prop-types';

// ROUTER & COOKIES
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { routes } from './routes';

// ELEMENTS
import Sidebar from './components/Sidebar';
import Header from './components/Header';
import Authorization from './hoc/authorization.jsx';

// REDUX
import { bindActionCreators } from 'redux'
import * as LogActions from './actions/LogActions';
import { connect } from 'react-redux';

class App extends Component {

  componentWillMount() {
    const email = localStorage.getItem('email');
    const access_token = localStorage.getItem('access_token');
    const username = localStorage.getItem('username');
    const role = localStorage.getItem('role');
    this.props.logActions.prepairAuth({email, access_token, username, role})
  }

  render() {
    const renderSwitch = () => (
      <Switch>
        {routes.map(route => {
          const component = route.isPrivate ? Authorization(route.component) : route.component;
          return (
            <Route
              key={route.path}
              exact={route.isExact}
              path={route.path}
              component={component}
            />
          );
        })}
      </Switch>
    )
    return (
      <Router>
        <div className="wrapper" style={{height: 'auto', minHeight: '100%'}}>
          <Sidebar/>
          <Header/>
          <div className="content-wrapper" style={{minHeight: 'calc(100vh - 50px)', display: 'flow-root'}}>
            {renderSwitch()}
          </div>
        </div>
      </Router>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    logActions: bindActionCreators(LogActions, dispatch),
  }
}

App.contextTypes = {
  router: PropTypes.object,
};

export default connect(null, mapDispatchToProps)(App)

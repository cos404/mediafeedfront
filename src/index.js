import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import App from './App.jsx'
import configureStore from './store'
import './index.css'

const store = configureStore()
render(
  <Provider store={store}>
    <div className='app'>
      <App/>
    </div>
  </Provider>,
  document.getElementById('root')
)

```
<section className="content">
  <div className="row">
    <div className="col-md-9">
      <div className="nav-tabs-custom">
        <ul className="nav nav-tabs">
          <li className="active"><a href="#" data-toggle="tab" aria-expanded="true">vk.com: Max</a></li>
          <li><a href="#" data-toggle="tab" aria-expanded="false">facebook.com: Cos</a></li>
          <li><a href="#" data-toggle="tab" aria-expanded="false">twitter.com: Mos</a></li>
        </ul>
        <div className="tab-content">
          <div className="active tab-pane">
            <div className="post">
              <div className="user-block">
                <img className="img-circle img-bordered-sm" src={userpic} alt="user image"/>
                <span className="username">
                  <a href="#">Jonathan Burke Jr.</a>
                  <a href="#" className="pull-right btn-box-tool"><FontAwesomeIcon icon={faTimes} className="fa" /></a>
                </span>
                <span className="description">Shared publicly - 7:30 PM today</span>
              </div>
              <p>
                Lorem ipsum represents a long-held tradition for designers, typographers and the like. Some people hate it and argue for its demise, but others ignore the hate as they create awesome tools to help create filler text for everyone from bacon lovers to Charlie Sheen fans.
              </p>
              <ul className="list-inline">
                <li>
                  <a href="#" className="link-black text-sm">
                    <FontAwesomeIcon icon={faShare} className="fa margin-r-5" />
                    Share
                  </a>
                </li>
                <li>
                  <a href="#" className="link-black text-sm">
                    <FontAwesomeIcon icon={faThumbsUp} className="fa margin-r-5" />
                    Like
                  </a>
                </li>
                <li className="pull-right">
                  <a href="#" className="link-black text-sm">
                    <FontAwesomeIcon icon={faComments} className="fa margin-r-5" />
                    Comments(5)
                  </a>
                </li>
              </ul>
              <input className="form-control input-sm" type="text" placeholder="Type a comment"/>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
```

```
<NavItem className="treeview" to="/a">
  <NavLink to="/a">
    <FontAwesomeIcon icon={faTachometerAlt} className="fa"/>
    <span>Dashboard</span>
    <span className="pull-right-container">
      <FontAwesomeIcon icon={faAngleLeft} className="fa pull-right-container" />
    </span>
  </NavLink>
  <ul className="treeview-menu" style={{display: 'none'}}>
    <li>
      <NavLink to="/b">
        <FontAwesomeIcon icon={faCircle} className="fa"/>
        Dashboard 1
      </NavLink>
    </li>
    <li>
      <NavLink to="/c">
        <FontAwesomeIcon icon={faCircle} className="fa" />
        Dashboard 2
      </NavLink>
    </li>
  </ul>
</NavItem>
<NavItem className="treeview" to="/a1">
  <NavLink to="/a1">
    <FontAwesomeIcon icon={faCopy} className="fa"/>
    <span>Dashboard</span>
    <span className="pull-right-container">
      <span className="label label-primary pull-right">4</span>
    </span>
  </NavLink>
  <ul className="treeview-menu" style={{display: 'none'}}>
    <li>
      <NavLink to="/b1">
        <FontAwesomeIcon icon={faCircle} className="fa"/>
        Dashboard 1
      </NavLink>
    </li>
    <li>
      <NavLink to="/c1">
        <FontAwesomeIcon icon={faCircle} className="fa" />
        Dashboard 2
      </NavLink>
    </li>
  </ul>
</NavItem>
```
